const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

const mongoClient = require('mongodb').MongoClient;
const mongoUrl = "mongodb://localhost:27017/mydb";

const mymongo = require("./mymongo");
const util = require("./util");

app.use(bodyParser.json());

const usersName = "users";
const locationsName = "locations";
const visitsName = "visits";

// Очищает базу данных.
function dropDataFromMongo() {
    mymongo.dropDataFromMongo(mongoClient, mongoUrl, usersName);
    mymongo.dropDataFromMongo(mongoClient, mongoUrl, locationsName);
    mymongo.dropDataFromMongo(mongoClient, mongoUrl, visitsName);
}

// Считывает данные из файла и сохраняет в базу данных.
function readJsonDataFromFile() {
    const users = JSON.parse(fs.readFileSync('users_1.json', 'utf8'));
    const locations = JSON.parse(fs.readFileSync('locations_1.json', 'utf8'));
    const visits = JSON.parse(fs.readFileSync('visits_1.json', 'utf8'));

    mymongo.insertToMongo(mongoClient, mongoUrl, users, usersName);
    mymongo.insertToMongo(mongoClient, mongoUrl, locations, locationsName);
    mymongo.insertToMongo(mongoClient, mongoUrl, visits, visitsName);
}

// 1) GET /<entity>/<id>
// http://127.0.0.1:3000/users/2
app.get('/:entity/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    const entity = req.params.entity;

    let result;

    try {
        if (entity === usersName) {
            result = await mymongo.getDataFromMongo(usersName, id, mongoClient, mongoUrl);
        } else if (entity === locationsName) {
            result = await mymongo.getDataFromMongo(locationsName, id, mongoClient, mongoUrl);
        } else if (entity === visitsName) {
            result = await mymongo.getDataFromMongo(visitsName, id, mongoClient, mongoUrl);
        } else {
            util.terminateUserError(res);
        }
    } catch (ex) {
        util.terminateServerError(res);
    }

    if (result.length === 0) {
        util.terminateUserError(res);
    }

    res.send(result);
});

// 2) GET /users/<id>/visits
//?fromDate=?&toDate=?&country=?&toDistance=?
app.get('/users/:id/visits', async (req, res) => {
    const userVisitsParameters = {
        userId: parseInt(req.params.id),
        fromDate: parseInt(req.param('fromDate')),
        toDate: parseInt(req.param('toDate')),
        country: req.param('country'),
        toDistance: parseInt(req.param('toDistance'))
    };

    try {
        const userVisitsResult = await mymongo.getUserVisits(
            userVisitsParameters, mongoClient, mongoUrl, visitsName, locationsName);
        res.json(userVisitsResult);
    } catch (ex) {
        util.terminateServerError(res);
    }
});

// 3) GET /locations/<id>/avg
//?fromDate=?&toDate=?&fromAge=?&toAge=?&gender=?
app.get('/locations/:id/avg', async (req, res) => {
    const locationsParameters = {
        locationId: parseInt(req.params.id),
        fromDate: parseInt(req.param('fromDate')),
        toDate: parseInt(req.param('toDate')),
        fromAge: parseInt(req.param('fromAge')),
        toAge: parseInt(req.param('toAge')),
        gender: req.param('gender')
    };

    try {
        const locationAVGResult = await mymongo.getLocationAVG(
            locationsParameters, mongoClient, mongoUrl, visitsName, usersName);

        res.json(locationAVGResult);
    } catch (ex) {
        util.terminateServerError(res);
    }
});

// 4) POST /<entity>/<id>
app.post('/:entity/:id', async (req, res) => {
    const id = parseInt(req.params.id);
    const entity = req.params.entity;

    // TODO: Check request data!

    const body = req.body;

    let result;

    try {
        if (entity === usersName) {
            result = await mymongo.updateDataToMongo(usersName, id, body, mongoClient, mongoUrl);
        } else if (entity === locationsName) {
            result = await mymongo.updateDataToMongo(locationsName, id, body, mongoClient, mongoUrl);
        } else if (entity === visitsName) {
            result = await mymongo.updateDataToMongo(visitsName, id, body, mongoClient, mongoUrl);
        } else {
            util.terminateUserError(res);
        }
    } catch (ex) {
        util.terminateServerError(res);
    }

    // TODO: check result!

    util.terminateSucces(res);
});

// 5) POST /<entity>/new
app.post('/:entity/new', async (req, res) => {
    const entity = req.params.entity;

    // TODO: Check request data!

    const body = req.body;

    let result;

    try {
        if (entity === usersName) {
            result = await mymongo.addDataToMongo(usersName, body, mongoClient, mongoUrl);
        } else if (entity === locationsName) {
            result = await mymongo.addDataToMongo(locationsName, body, mongoClient, mongoUrl);
        } else if (entity === visitsName) {
            result = await mymongo.addDataToMongo(visitsName, body, mongoClient, mongoUrl);
        } else {
            util.terminateUserError(res);
        }
    } catch (ex) {
        util.terminateServerError(res);
    }

    // TODO: check result!

    util.terminateSucces(res);
});

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    // Drop database.
    dropDataFromMongo();

    // Create database.
    readJsonDataFromFile();

    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        const worker = cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);

        // Recreate worker if it was killed.
        cluster.fork();
    });
} else {
    app.listen(3000, function () {
        console.log('Example app listening on port 3000!')
    });

    console.log(`Worker ${process.pid} started`);
}