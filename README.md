# README #

### What is this repository for? ###

An example of a nodejs server for [HighLoad Cup 2017](https://highloadcup.ru/)

### How do I get set up? ###

* Download and unpack node-v8.5.0-linux-x64
* Download and unpack mongodb-linux-x86_64-ubuntu1604-3.4.9
* Install all required nodejs dependencies.
* Install supervisor: 
```
npm install supervisor -g
```
* Add node to the PATH:
```
export PATH=/path_to_node/node-v8.5.0-linux-x64/bin:$PATH
```

* run mongo:
```
sudo ./mongod --dbpath /path_to_mongo_tmp_folder/mongo_db
```
* run server:
```
node app.js
```
* or:
```
supervisor app.js
```
* Test the server with help of GET request:
```
http://127.0.0.1:3000/users/1
```