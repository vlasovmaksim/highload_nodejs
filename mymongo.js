const util = require("./util");
const moment = require('moment');

exports.dropDataFromMongo = async function dropDataFromMongo(client, url, tableName) {
    try {
        let db = await client.connect(url);
        let table = await db.collection(tableName);
        let result = await table.drop();
        db.close();

        console.log(`collection ${tableName} deleted`);

        return result;
    } catch (ex) {
        console.log(ex);

        throw ex;
    }
};

exports.getDataFromMongo = async function getDataFromMongo(tableName, id, client, url) {
    try {
        let db = await client.connect(url);
        let table = await db.collection(tableName);
        let result = await table.find({ id: id }).toArray();
        db.close();

        return result;
    } catch (ex) {
        console.log(ex);

        throw ex;
    }
};

exports.addDataToMongo = async function addDataToMongo(tableName, body, client, url) {
    try {
        let db = await client.connect(url);
        let table = await db.collection(tableName);
        let result = await table.insertOne(body);

        console.log(result);
        db.close();

        return result;
    } catch (ex) {
        console.log(ex);

        throw ex;
    }
};

exports.updateDataToMongo = async function updateDataToMongo(tableName, id, body, client, url) {
    try {
        let db = await client.connect(url);
        let table = await db.collection(tableName);

        const myquery = { id: id };
        let newvalue = body;
        newvalue["id"] = id;

        let result = await table.updateOne(myquery, newvalue);

        console.log(result);
        db.close();

        return result;
    } catch (ex) {
        console.log(ex);

        throw ex;
    }
};

exports.insertToMongo = async function insertToMongo(client, url, data, tableName) {
    try {
        let db = await client.connect(url);
        let table = await db.collection(tableName);
        let result = await table.insertMany(data[tableName]);
        console.log("Number of documents inserted: " + result.insertedCount);
        db.close();
    } catch (ex) {
        console.log(ex);
    }
};

exports.getUserVisits = async function getUserVisits(parameters, client, url, visitsName, locationsName) {
    let userVisitsResult = {
        "visits": []
    };

    try {
        let db = await client.connect(url);
        let visitsTable = await db.collection(visitsName);
        let resultVisits = await visitsTable.find({user: parameters.userId}).toArray();

        for (let i = 0; i < resultVisits.length; i++) {
            const tmpVisited = resultVisits[i];

            //filter by fromDate, toDate
            const visited_at = tmpVisited.visited_at;

            if (!isNaN(parameters.fromDate)) {
                if (visited_at < parameters.fromDate) {
                    continue;
                }
            }

            if (!isNaN(parameters.toDate)) {
                if (visited_at > parameters.toDate) {
                    continue;
                }
            }

            const locationId = tmpVisited.location;

            let locationsTable = await db.collection(locationsName);
            let resultLocations = await locationsTable.find({id: locationId}).toArray();

            const tmpLocation = resultLocations[0];

            //filter by country, toDistance
            if (parameters.country !== undefined) {
                if (tmpLocation["country"] !== parameters.country) {
                    continue;
                }
            }

            if (!isNaN(parameters.toDistance)) {
                if (tmpLocation["distance"] > parameters.toDistance) {
                    continue;
                }
            }

            userVisitsResult["visits"].push(
                {
                    "mark": tmpVisited["mark"],
                    "visited_at": visited_at,
                    "place": tmpLocation["place"]

                }
            );
        }
    } catch (ex) {
        console.log(ex);

        throw ex;
    }

    return userVisitsResult;
};

exports.getLocationAVG = async function getLocationAVG(parameters, client, url, visitsName, usersName) {
    let locationAVG = {
        "avg": 0.0
    };

    try {
        const db = await client.connect(url);
        const visitsTable = await db.collection(visitsName);
        const resultVisits = await visitsTable.find({location: parameters.locationId}).toArray();

        let marks = 0.0;
        let marksCount = 0;

        for (let i = 0; i < resultVisits.length; i++) {
            const tmpVisited = resultVisits[i];

            //filter by fromDate, toDate
            const visited_at = tmpVisited.visited_at;

            if (!isNaN(parameters.fromDate)) {
                if (visited_at < parameters.fromDate) {
                    continue;
                }
            }

            if (!isNaN(parameters.toDate)) {
                if (visited_at > parameters.toDate) {
                    continue;
                }
            }

            const userId = tmpVisited.user;

            const usersTable = await db.collection(usersName);
            const resultUsers = await usersTable.find({id: userId}).toArray();

            const resultUser = resultUsers[0];

            //filter by gender
            if (parameters.gender !== undefined) {
                if (resultUser["gender"] !== parameters.gender) {
                    continue;
                }
            }

            //filter by age
            let startDate = moment(new Date(resultUser["birth_date"]));
            let endDate = moment(Date.now());
            let remainingDate = moment(endDate).diff(startDate, 'years');

            if (!isNaN(parameters.fromAge)) {
                if (remainingDate < parameters.fromDate) {
                    continue;
                }
            }

            if (!isNaN(parameters.toAge)) {
                if (remainingDate > parameters.toDate) {
                    continue;
                }
            }

            // Calculate the result
            marks += tmpVisited.mark;
            marksCount++;
        }

        // Normalize the result
        marks /= marksCount;

        locationAVG["avg"] = marks;
    } catch (ex) {
        console.log(ex);

        throw ex;
    }

    return locationAVG;
};