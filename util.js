exports.terminateUserError = function (res) {
    res.status(404).end();
};

exports.terminateServerError = function (res) {
    res.status(500).end();
};

exports.terminateSucces = function (res) {
    res.status(200).end();
};